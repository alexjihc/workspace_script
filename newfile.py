#!/usr/bin/env python
# Author: Alex Choi

import os
import re
import sys
import argparse
import pathlib

######## EXAMPLE ###########
# python newfile.py [-n Names] [-t Components] module
# ex. python newfile.py -n Test -t mc maintenance
#
# option argument: component e.g. -n Test [required]
# option argument: component e.g. -t m [maint], -t mcfbg [all components], -t mb [maint and browse]
#
# position argument: module e.g. maintenance inventory finance [required]
#
######## END EXAMPLE ########

### SET PROPERTIES ###
WEBUI_DIR = 'erp-assetmgmt-webui'

# Set Typescript directory
TS_RESOURCE_DIR = 'src/main/resources/META-INF/resources/resources/ts/com/qad/erp/assetmgmt/'

# Set File Name and Extension
FILE_EXTENSION = 'ts'

TS_MAINT     = 'Maint'
TS_CONSTANT  = 'Constants'
TS_VIEW_FORM = 'ViewForm'
TS_BROWSE    = 'Browse'
TS_GRID      = 'Grid'

### END SET PROPERTIES ###

# File and Directory Manipulation
TS_DIRECTORY = os.path.join(WEBUI_DIR, TS_RESOURCE_DIR)

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--name', dest='names', help="name of ts file [Required]")
parser.add_argument('-t', '--template', dest='components', help="create ts file e.g. m: maint, c: constants, f: form, b: browse, g: grid")
parser.add_argument('module', help="specify module directory e.g. maintenance, inventory, finance ")
# parser.add_argument('-c', '--constants', action='store_true', help="create constants ts file")
# parser.add_argument('-f', '--form', action='store_true', help="create view form ts file")
# parser.add_argument('-b', '--browse', action='store_true', help="create browse ts file")
# parser.add_argument('-g', '--grid', action='store_true', help="create grid ts file")

args = parser.parse_args()

MODULE_DIR = os.path.join(TS_DIRECTORY, args.module)
TS_PATH = os.path.abspath(MODULE_DIR)

if args.names != None:
    TS_MAINT_FILE     = args.names + TS_MAINT + "." + FILE_EXTENSION
    TS_CONSTANTS_FILE = args.names + TS_CONSTANT + "." + FILE_EXTENSION
    TS_VIEW_FORM_FILE = args.names + TS_VIEW_FORM + "." + FILE_EXTENSION
    TS_BROWSE_FILE    = args.names + TS_BROWSE + "." + FILE_EXTENSION
    TS_GRID_FILE      = args.names + TS_GRID + "." + FILE_EXTENSION

    file = pathlib.Path(TS_PATH)
    if file.exists():
        ts_maint_path     = os.path.join(TS_PATH, TS_MAINT_FILE)
        ts_constants_path = os.path.join(TS_PATH, TS_CONSTANTS_FILE)
        ts_view_form_path = os.path.join(TS_PATH, TS_VIEW_FORM_FILE)
        ts_browse_path    = os.path.join(TS_PATH, TS_BROWSE_FILE)
        ts_grid_path      = os.path.join(TS_PATH, TS_GRID_FILE)

        for component in args.components:
            if component != None:
                if component.lower() == "m":
                    with open (ts_maint_path, 'w') as nf:
                        nf.close()
                elif component.lower() == "c":
                    with open (ts_constants_path, 'w') as nf:
                        nf.close()
                elif component.lower() == "f":
                    with open (ts_view_form_path, 'w') as nf:
                        nf.close()
                elif component.lower() == "b":
                    with open (ts_browse_path, 'w') as nf:
                        nf.close()
                elif component.lower() == "g":
                    with open (ts_grid_path, 'w') as nf:
                        nf.close()

    else:
        print("{} does not exist.".format(args.module))
else:
    print("{} cannot be empty.".format(args.name))


